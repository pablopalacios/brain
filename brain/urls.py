from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^export/python/$', 'data.views.export_to_pybrain'),
    url(r'^admin/', include(admin.site.urls)),
)
