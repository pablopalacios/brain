"""
Django settings for brain project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

ADMINS = (
    ('Pablo Palacios', 'pablo@sleepy.com.br'),
)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'd^6+&j&ds3oa9z_fg!w2amo=7d8z0(cy(7of6c^ij5_5g5#3uh'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['brain.pablopalacios.me']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'data',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'brain.urls'

WSGI_APPLICATION = 'brain.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    #    'ENGINE': 'django.db.backends.postgresql_psycopg2', 
    #    'NAME': 'brain',
    #    'USER': 'django_user',
    #    'PASSWORD': 'nZRMv8q0',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'pt-BR'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
#STATIC_ROOT = '/home/plabo/webapps/brain_static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'data/templates')]

EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'sleepy'
EMAIL_HOST_PASSWORD = 'nZRMv8q0'
DEFAULT_FROM_EMAIL = 'pablo@sleepy.com.br'
SERVER_EMAIL = 'pablo@sleepy.com.br'
