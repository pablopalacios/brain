# -*- coding: utf-8 -*-
from django.db import models


CLASSES = ["Peixes", "Anfíbios", "Répteis", "Aves", "Mamíferos"]

CLASSES_CHOICES = (
    ("PE", "Peixes",),
    ("AN", "Anfíbios"),
    ("RE", "Répteis"),
    ("AV", "Aves"),
    ("MA", "Mamíferos"),
)

CLASSES_COD = {classe[0]:cod for cod, classe in enumerate(CLASSES_CHOICES)}

HABITAT = (
    ("AG", "Água"),
    ("AT", "Água/Terra"),
    ("TE", "Terra"),
    ("AR", "Ar")
)

RESPIRACAO = (
    ("BR", "Branquial"),
    ("CU", "Cutânea"),
    ("CP", "Cutânea/Pulmonar"),
    ("PU", "Pulmonar")
)

LOCOMOCAO = (
    ("NA", "Nadadeiras"),
    ("ND", "Nada (ex. rastejo)"),
    ("PA", "Patas"),
    ("AS", "Asas"),
)

REPRODUCAO = (
    ("SE", "Sexuada"),
    ("AS", "Assexuada")
)

SANGUE = (
    ("FR", "Sangue frio"),
    ("QU", "Sangue Quente")
)

COBERTURA = (
    ("ES", "Escamas"),
    ("PU", "Pele úmida"),
    ("PS", "Pele seca"),
    ("PL", "Pelos"),
    ("PN", "Penas"),
)


class Animal(models.Model):
    nome = models.CharField(max_length=100)
    habitat = models.CharField(max_length=2, choices=HABITAT)
    respiracao = models.CharField("Respiração",
                                  max_length=2,
                                  choices=RESPIRACAO
    )
    locomocao = models.CharField("Locomoção", max_length=2, choices=LOCOMOCAO)
    reproducao = models.CharField(
        "Reprodução",
        max_length=2,
        choices=REPRODUCAO
    )
    sangue = models.CharField("Sangue", max_length=2, choices=SANGUE)
    cobertura = models.CharField(max_length=2, choices=COBERTURA)
    classe = models.CharField(max_length=2, choices=CLASSES_CHOICES)
    teste = models.BooleanField("Grupo de teste?", default=False)
    habitat_cod = models.DecimalField(
        "COD Habitat",
        max_digits=3,
        decimal_places=2,
        blank=True,
        default=0
    )
    respiracao_cod = models.DecimalField(
        "COD Respiração",
        max_digits=3,
        decimal_places=2,
        blank=True,
        default=0
    )
    locomocao_cod = models.DecimalField(
        "COD Locomoção",
        max_digits=3,
        decimal_places=2,
        blank=True,
        default=0
    )
    reproducao_cod = models.DecimalField(
        "COD Reprodução",
        max_digits=3,
        decimal_places=2,
        blank=True,
        default=0
    )
    sangue_cod = models.DecimalField(
        "COD Sangue",
        max_digits=3,
        decimal_places=2,
        blank=True,
        default=0
    )
    cobertura_cod = models.DecimalField(
        "COD Cobertura",
        max_digits=3,
        decimal_places=2,
        blank=True,
        default=0
    )
    
    def as_array(self):
        attrs = [
            self.habitat_cod,
            self.respiracao_cod,
            self.locomocao_cod,
            self.reproducao_cod,
            self.sangue_cod,
            self.cobertura_cod,
        ]
        return [float(a) for a in attrs]

    def target(self):
        return [CLASSES_COD[self.classe]]

    def get_class_name(self):
        for cod, name in CLASSES_CHOICES:
            if cod == self.classe:
                return name
    
    def __unicode__(self):
        return self.nome

    class Meta:
        verbose_name_plural = "Animais"
        ordering = ["nome"]
