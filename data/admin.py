from django.contrib import admin
from data.models import Animal


def mark_test_group(modeladmin, request, queryset):
    queryset.update(teste=True)
mark_test_group.short_description = "Incluir animais no grupo de teste"


def mark_train_group(modeladmin, request, queryset):
    queryset.update(teste=False)
mark_train_group.short_description = "Remover animais do grupo de treino"


@admin.register(Animal)
class Animal_Admin(admin.ModelAdmin):
    fields = (
        ("nome", "classe", "teste"),
        ("habitat", "habitat_cod"),
        ("respiracao", "respiracao_cod"),
        ("locomocao", "locomocao_cod"),
        ("reproducao", "reproducao_cod"),
        ("sangue", "sangue_cod"),
        ("cobertura", "cobertura_cod")
    )
<<<<<<< HEAD
    list_display = ["nome", "classe", "teste", "habitat_cod", "respiracao_cod", "locomocao_cod", "reproducao_cod", "sangue_cod", "cobertura_cod"]
    list_filter = ("classe", "teste", "habitat","respiracao","locomocao","reproducao","sangue","cobertura")
=======
    list_display = ["nome", "classe", "teste", "habitat_cod", "respiracao_cod",
                    "locomocao_cod", "reproducao_cod", "sangue_cod",
                    "cobertura_cod"]
    list_filter = ("classe", "habitat", "respiracao", "locomocao",
                   "reproducao", "sangue", "cobertura")
>>>>>>> dev
    search_fields = ("nome",)
    actions = [mark_test_group, mark_train_group]

    class Media:
        js = ("data/js/animal.js",)
