var attributes = {
    "habitat" : {
	"AG" : -1.0,
	"AT" : -0.5,
	"TE" : 0.0,
	"AR" : 1.0,
    },

    "respiracao" : {
	"BR" : -1.0,
	"CU" : 0.0,
	"CP" : 0.5,
	"PU" : 1.0,
    },

    "locomocao" : {
	"NA" : -1.0,
	"ND" : 0.0,
	"PA" : 0.5,
	"AS" : 1.0,
    },

    "reproducao" : {
	"SE" : 1.0,
	"AS" : -1.0,
    },

    "sangue" : {
	"FR" : -1.0,
	"QU" : 1.0,
    },

    "cobertura" : {
	"ES" : -1.0,
	"PU" : -0.5,
	"PS" : 0.0,
	"PL" : 0.5,
	"PN" : 1.0,
    },
};

function updateValue(obj){
    function f(){
	var attr = obj.name;
	var value = obj.value;
	obj_cod = document.getElementById(obj.id + "_cod");
	obj_cod.value = attributes[attr][value];
    };
    return f;
};

function add_events(){
    var elements = ["id_habitat", "id_respiracao", "id_locomocao", "id_reproducao", "id_sangue", "id_cobertura"];
    for (element in elements){
	e = document.getElementById(elements[element]);
	e.addEventListener("change", updateValue(e), false);
    };
};
window.onload = add_events;
