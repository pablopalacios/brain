# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.shortcuts import render

<<<<<<< HEAD
# from pybrain.datasets import ClassificationDataSet
# from pybrain.tools.shortcuts import buildNetwork
# from pybrain.structure.modules import SoftmaxLayer
# from pybrain.supervised.trainers import BackpropTrainer

# Dataset data
# N_CLASSES = len(CLASSES) # número de classes
# N_TARGET = 1  # número de respostas possíveis por entrada
# N_FEATURES = 6 # número de características por dado

# Network data
# IN = [N_FEATURES] 
# HIDDEN = [5] 
# OUT = [N_CLASSES]
# LAYERS = IN + HIDDEN + OUT
# OUTCLASS = SoftmaxLayer

# Train data
# N_TREINOS = 20 # Quantidade de treinos a serem feitos
# N_EPOCAS = 5 # Quantidade de "treinamentos" por treinos
=======
from data.models import Animal, CLASSES
from brain import make_dataset, make_and_train_network

>>>>>>> dev

def home(request):
    return HttpResponseRedirect('/admin/')


def export_to_pybrain(request):
    test_animals = Animal.objects.filter(teste=True)
    train_animals = Animal.objects.filter(teste=False)
    return render(request, "pybrain.html", {
        "test_animals":test_animals,
        "train_animals":train_animals
    })


<<<<<<< HEAD
# def web_brain(request):
#     train_animals = Animal.objects.filter(teste=False)
#     ds = make_dataset(train_animals)
#     network = make_and_train_network(ds)
# 
#     results = []
#     test_animals = Animal.objects.filter(teste=True)
#     for animal in test_animals:
#         result = ds.getClass(network.activate(animal.as_array()).argmax())
#         results.append((animal.nome, animal.get_class_name(), result))
#     return render(request, "results.html", {"animals":results})
        
# def make_and_train_network(dataset):
#     network = buildNetwork(*LAYERS, outclass=OUTCLASS)
#     trainer = BackpropTrainer(
#         network,
#         dataset=dataset,
#         momentum=0.1,
#         weightdecay=0.01
#     )
#     for treino in range(N_TREINOS):
#         trainer.trainEpochs(N_EPOCAS)
#     return network
    
# def make_dataset(queryset):
#     ds = ClassificationDataSet(N_FEATURES,
#         N_TARGET,
#         nb_classes=N_CLASSES,
#         class_labels=CLASSES
#     )

#     for data in queryset:
#         in_data = data.as_array()
#         out_data = data.target()
#         ds.addSample(in_data, out_data)
#     ds._convertToOneOfMany()
#     return ds
=======
def test_data(request):
    train_animals = Animal.objects.filter(teste=False)
    ds = make_dataset(train_animals)
    network = make_and_train_network(ds)

    results = []
    test_animals = Animal.objects.filter(teste=True)
    for animal in test_animals:
        result = ds.getClass(network.activate(animal.as_array()).argmax())
        results.append((animal.nome, animal.get_class_name(), result))
    return render(request, "results.html", {"animals":results})
>>>>>>> dev
